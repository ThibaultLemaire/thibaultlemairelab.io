{ pkgs ? import <nixpkgs> { } }:

with pkgs;

dockerTools.buildLayeredImage {
  name = "registry.gitlab.com/thibaultlemaire/thibaultlemaire.gitlab.io/pages";
  tag = "latest";

  contents = [ busybox zola imagemagick ];

  config.Cmd = [ "/bin/sh" ];
}
