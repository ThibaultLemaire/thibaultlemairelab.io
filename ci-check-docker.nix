{ pkgs ? import <nixpkgs> { } }:

with pkgs;

dockerTools.buildLayeredImage {
  name = "registry.gitlab.com/thibaultlemaire/thibaultlemaire.gitlab.io/check";
  tag = "latest";

  contents = [ busybox zola nixfmt ];

  config.Cmd = [ "/bin/sh" ];
}
