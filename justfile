build:
    zola build

serve:
    zola serve

check:
    zola check
    nixfmt --check *.nix

# Format nix files
format-nix:
    nixfmt *.nix

# Build docker image for CI/CD. Note that *you need docker installed*. I would add docker to the nix shell, but you need the docker daemon installed on your system anyway
build-check-docker:
    nix build --file ci-check-docker.nix
    docker load < result
    rm result

build-pages-docker:
    nix build --file ci-pages-docker.nix
    docker load < result
    rm result

# Push CI/CD docker image to Gitlab container registry
push-docker user pass: format-nix build-check-docker build-pages-docker
    docker login registry.gitlab.com -u {{user}} -p {{pass}}
    docker push registry.gitlab.com/thibaultlemaire/thibaultlemaire.gitlab.io/check
    docker push registry.gitlab.com/thibaultlemaire/thibaultlemaire.gitlab.io/pages
