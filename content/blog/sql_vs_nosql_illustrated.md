+++
title = "SQL vs. NoSQL Illustrated"
date = 2021-08-04

[taxonomies]
tags = ["MongoDB", "NoSQL"]
+++

So I made this as part of a talk I gave at work, and thought it might be useful to someone else.

![An abstract representation of SQL vs. NoSQL databases](/blog/SQL-vs-NoSQL-Illustrated.svg)

(You can right-click on the image to download it or open it in a new tab. It _is_ the source SVG.)

## Description

The idea is that in a NoSQL | Non-Structured | Non-Relational | Schemaless database like MongoDB you are free to put anything you want in your documents.

So, conceptually, they are like bags hooked by their ID (or other indexes), whereas a common SQL database looks more like shelves with dedicated slots where you can only put a given type of data.
(Concretely: float, int, string, etc.)

I think the bag analogy also gives a nice sense of why querying or sorting on non-indexed fields in MongoDB can be quite expensive (you have to rummage all the bags).

## License

This illustration is [CC-BY](https://creativecommons.org/licenses/by/4.0/) Thibault Lemaire, made with Inkscape from the following public domain resources:

- [Gray bag](https://freesvg.org/gray-bag)
- [Rope](https://freesvg.org/rope-143437)
- [Small red scissors line art vector illustration](https://freesvg.org/small-red-scissors-line-art-vector-illustration)
- [Shelves](https://freesvg.org/shelves)
- [bait](https://freesvg.org/bait)
- [ABC blocks vector image](https://freesvg.org/abc-blocks-vector-image)
