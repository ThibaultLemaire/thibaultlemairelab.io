+++
title = "Hosting a LAN Party with Libre Games and NixOS"
date = 2022-12-31

[taxonomies]
tags = ["Event", "LAN", "Linux", "NixOS"]
+++

Earlier this year (2022, which will come to an end in the next hours as I'm writing this), on week 42, [Rézine celebrated its 10th anniversary](https://www.rezine.org/10ans)!

What's Rézine you ask?
It's a local ISP association that I'm a member of.

That's right!
"ISP association" as in: a bunch of volunteers like you or I who decided to become an Internet Service Provider!
Yes, that's a thing, and yes that's awesome!
Or at least it's a thing in France, although we are but a tiny spec compared to for-profit ISP companies.
(I'd be curious to know if other such initiatives exist in other countries!)

But as much as I love it, Rézine isn't what I'm here to write about.
I'm here to write specifically about a LAN party I organised as part of the celebration.

If you don't know what that is either, a LAN party is an event where people meet in person to play video games together over a Local Area Network.

Here's a picture I took at the event with the crappy camera of my Fairphone 2:

![People playing on their laptops at a LAN Party](/blog/LAN-Party.jpg)

## The Goal

> Anyone can come in with any hardware and be able to play at least one game

With bonus points if:

- Participants don't have to create an account anywhere
- Participants don't have to install anything if they don't want to

## Means

So, picking free software games -- while not strictly necessary -- helps a lot with the main goal.

Take Minetest for example.
There are clients for Linux, Windows, macOS, and _Android_.
Meaning you can litterally grab people _off the street_ to join your LAN on their __phones__!¹
(Don't actually do that, harassing people on the street is rude!)
Heck, now that I think about it, you could even make an unprepared LAN party in a remote chalet completely cut off from any internet connection, provided you already have Minetest on your phone!²

The first bonus objective is also granted for free (!) with libre games as the very few I know that _do_ require accounts, only do so for online play, not local play.
(Again, having access to the source code is not a requirement for this, only a strong correlation.)

Now, the second bonus objective is really where it's at, and __this is where NixOS comes in__.

Though before diving into that, I'd like to point out one more interesting trick to reach the main goal (but feel free to skip to the next section):

With turned-based games like Hedgewars or The Battle for Wesnoth, or games that feature split-screen like Stuntrally, it is even possible for participants to come _without any hardware at all_, and play on others' machines.

¹: The client has the same capabilities on every platform.
You can join a LAN game hosted on a laptop from your phone, and vice-versa!

²: You can start an Android hotspot (even if you don't have an internet connection), share Minetest over F-droid, and voilà: Anyone with a smartphone can play with you!
This also applies to Mindustry and SuperTuxKart!

### A live NixOS system for LAN Gaming

In the lingo of Linux distros, a live system is an operating system that resides solely in RAM.
That means you don't need a hard-drive to use it (or if you do, that this drive will only ever be read from by the system, never written to);
And all the changes you make to it (e.g. installing software) are ephemeral.
You can literally rip out the power cord from your machine (or battery if you're using a laptop), and it will be like nothing has ever happened.
(You don't actually _need_ to rip anything out, simply turning it off will do just as well.)

And this is just perfect for us since we want to let people play without installing anything, remember?
(They might have a very old system where it's hard to install a compatible version of the game, or not enough disk space, etc.)

Now the only problem with a live system is, well, that it's live.
Either everything has to come preloaded, or you have to download it from somewhere.

Even with the few games I selected, pre-loading them would make the system image impractically large, so I decided to make the live system dynamically download the games from a host machine in the local network.
(Which should be an order of magnitude faster than downloading from the Internet.)

To build such a live system, I decided to use [NixOS](https://nixos.org), which is a Linux distro based on the Nix package manager.

Nix is awesome for a number of reasons, but in this context it is specifically awesome because it lets me share my work with you, so that you too can learn and leverage the power of netbooting and/or binary caches!

If you want to know more, check out [my repository](https://gitlab.com/ThibaultLemaire/lan-party-with-libre-games-and-nixos), where I have documented everything in detail.
(And feel free to reach out if I missed something and/or something is unclear.)

(Example of reusable parts: The same way I serve games to guest machines over a local network, you can distribute locally built software inside a company network, so your coworkers use the exact same version you do, and they don't have to build it themselves.)
