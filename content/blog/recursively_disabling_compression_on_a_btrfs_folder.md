+++
title = "Recursively disabling compression on a folder in a BTRFS"
date = 2023-05-24

[taxonomies]
tags = ["Linux", "Compression", "BTRFS"]
+++

Let's say you have enabled (heavy) compression on your BTRFS partition, but you have a folder for e.g. compilation that is touched very often.

You'd like to disable compression for that folder (and all of its content) to speed things up:

``` console
❯ find . -exec sudo btrfs property set '{}' compression none \;
```

I might be an idiot, but I'm pretty sure I won't remember this next time I need it, and I couldn't find anything online, so here it is.
