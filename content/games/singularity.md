+++
title = "Singularity"
date = 2022-02-06

[taxonomies]
tags = ["Godot", "WASM", "GDScript", "GLSL", "Top-down", "2D", "Browser-based", "Puzzle", "GGJ"]
+++

[🔎 SOURCE][source] | [🕹️ PLAY][game] | [🌍 Global Game Jam][GGJ page]

[source]: https://gitlab.com/Tuara/singularity
[game]: https://tuara.gitlab.io/singularity
[GGJ page]: https://globalgamejam.org/2022/games/singularity-9-0

Singularity is a game we made in two days for the GGJ 2022 with my friend Thomas.

It's a 2D physics-based puzzle game which is designed to be self-explanatory, so you can just hit [that PLAY button][game] right now ;)

This year, we were 5 teams at the Toulouse jam site to have chosen Godot. Hurrah for Free Software!

I took the occasion to try out Godot shaders (which are basically just GLSL geared for Godot), and while my shader skills aren't that incredible (3h for a simple square...) the experience was a fun one.

If you want to know more, I'll forward you to [the README][source] where I touch a bit more on diversifiers.
