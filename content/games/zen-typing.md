+++
title = "Zen Typing"
date = 2020-10-10

[taxonomies]
tags = ["Rust", "WASM", "Typing", "Browser-based"]
+++

[🔎 SOURCE][source] | [🕹️ PLAY](/zen-typing)


Zen Typing is a casual game I made as a recreative introduction to [touch typing].

## Gameplay

The gameplay is simple: There is a series of symbols (called glyphs) on screen, each corresponding to a given key on your keyboard, and you have to hit them in order, from bottom to top.
Each time you successfully hit a glyph, it is replaced by the one above it and a new one appears at the top of the screen, ad infinitum.

The virtual keyboard at the bottom of your screen is here to help you figure out and visualize the mapping of glyphs to your physical device.
Try and hit some keys to see which get triggered on your screen.

The gameplay might remind you of [Guitar Hero], except with 31 keys instead of 5, and you can go at your own pace.

## Placing your fingers

> You don't need to have read this section to play the game.
In fact, it helps to have at least 5 seconds of gameplay by this point.

Placing your fingers on your keyboard is the first step to touch typing, and arguably the only one that matters.
The entirety of the game has been designed around that core concept and helping you build that mental map and kick start your muscle memory.

You'll notice every glyph is made of a simple geometrical shape, sometimes inscribed with an arrow.
Each shape denotes a finger.

- ⬡ (hexagon) is the index finger
- ⬠ (pentagon) is the middle finger
- ◇ (diamond) is the ring finger
- △ (triangle) is the little finger

The arrow, if present, indicates the direction in which the finger is supposed to move from its rest position.

Glyphs for the left hand appear on the left half of the screen, glyphs for the right hand appear on the right half of the screen.

To place your fingers at their rest position (a.k.a "start position") you should seek two little bumps on your keyboard (on a QWERTY, QWERTZ, or AZERTY keyboard, they are on the letters `F` and `J`, on a Dvorak keyboard, they are on the letters `U` and `H`), this is where you should place your index fingers.
The other fingers are placed horizontally on the keys directly adjacent of the same row.
This is called the [home row] and corresponds to the empty symbols (without an arrow inscribed).

The spacebar ( ␣ ) can be hit by either of your thumbs (left or right), but try to stick to the same thumb once you have chosen one you prefer.

## Why the weird shapes?

I have been asked why I went and invented my own symbols instead of simply using letters.
It's true: it means learning a whole new set of 31 symbols, I know, but I had my reasons.
The purpose of these symbols is two-fold:

1. Whatever your language, layout, or even keyboard, they're the same. No configurations. No translations.
2. They give you visual cues as to where to place your fingers, and how to move them.

In fact, 2. is the whole point of the game.
Ultimately, anyone should be able to play it without even reading this post. To ge even further, the following information should be the *absolute minimum* you need to know in order to play the game:

- It's a game
- You play it with a keyboard

Everything else should be something you can figure out by yourself.
I would even argue that using a keyboard is something you can figure out just by looking at the virtual keyboard, but admittedly it's not so obvious (yet).

Additionally, I never meant for the game to be yet an other typing game.
I wanted it to fill a gap in the learning process: That moment when you start to learn touch typing, but you have to painfully force yourself to place your fingers correctly, stop at every letter to think which finger should move, not look at the keyboard to train your muscle-memory ...
That part was no fun.
I wanted it to be fun.

Because looking at the keyboard won't help you, you just won't look at the keyboard, and you'll naturally learn to type without looking. That's actually what I learnt when learning Dvorak.

## Conclusion

In short, this game is only meant as an introduction.
Once you feel comfortable with finger placement, you can move on to other training games or software with dedicated training for your language and layout.
It's also something you can play casually whenever you have a few moments to kill and/or you feel like it, and stop any time you want.


[Guitar Hero]: https://en.wikipedia.org/wiki/Guitar_Hero#Gameplay
[touch typing]: https://en.wikipedia.org/wiki/Touch_typing
[home row]: https://en.wikipedia.org/wiki/Touch_typing#Training
[source]: https://gitlab.com/ThibaultLemaire/zen-typing
