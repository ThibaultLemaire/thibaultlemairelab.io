
+++
title = "Pong"
date = 2019-12-22

[taxonomies]
tags = ["Godot", "WASM", "GDScript", "2D", "2 Players", "Browser-based"]
+++

[🔎 SOURCE][source] | [🕹️ PLAY](/godot)

This is [one of the Godot demo projects][2D Pong] that I built for the web using the built-in HTML5 target of Godot.
The purpose was to have a very basic working example of a Godot game hosted on Gitlab Pages.

It's just a simple Pong game without even a score count.

Players can move by clicking or touching the left and right halves of their area, or by using `A` & `Z` for the cyan player and `↑` & `↓` for the purple player.

It can be a quick game to play on a phone with a friend on the bus or subway.

---

### Tech trivia

Godot doesn't exactly compile your games to WASM.
It's the engine itself that has been compiled to WASM, and is able to interpret GDScript.
So all the editor does is pack your assets and scripts and ship them along with that WASM version of the engine.

This makes the process of building a web version of your game very fast ([the CI runs in <1min][Pong CI]), but the resulting bytecode relatively heavy ([~12MB][Pong .wasm]), and the performance not so good.
Compare that to the [~3min compile time of Zen Typing][Zen Typing CI] (~1min30s cached), and the [720KB][Roguelike .wasm] of the [Roguelike tutorial].

[source]: https://gitlab.com/ThibaultLemaire/godot
[2D Pong]: https://github.com/godotengine/godot-demo-projects/tree/master/2d/pong
[Pong CI]: https://gitlab.com/ThibaultLemaire/godot/-/pipelines
[Pong .wasm]: https://thibaultlemaire.gitlab.io/godot/index.wasm
[Zen Typing CI]: https://gitlab.com/ThibaultLemaire/zen-typing/-/pipelines
[Roguelike .wasm]: https://thibaultlemaire.gitlab.io/roguelike-tutorial/roguelike-doryen.wasm
[Roguelike tutorial]: /games/roguelike-tutorial
