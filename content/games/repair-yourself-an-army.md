
+++
title = "Repair Yourself an Army"
date = 2020-04-12

[taxonomies]
tags = ["Godot", "WASM", "GDScript", "Top-down shooter", "2D", "Browser-based", "GGJ"]
+++

[🔎 SOURCE][source] | [🕹️ PLAY](/repairyourselfanarmy) | [🌍 Global Game Jam][GGJ page]

This is a game I made with the [Godot] engine in 26 (almost) consecutive hours for the Global Game Jam 2020.
The theme was "Repair".

![screenshot](/repairyourselfanarmy/banner.png)

Here is the original description I wrote for the GGJ entry:

> You are a rebel repair bot caught in the crossfire of a tripartite war.
> Repair the broken bots to turn them against the invaders.
>
> You are equipped with a gun and repair field, but don't count on your gun too much as it's not very efficient.
> Instead you should focus on scavenging the broken bots across the battlefield and grow your rebel army.
>
> WASD to move, mouse to aim and shoot, scrollwheel to zoom.
>
> Smoking bots are broken and won't attack you, those are your preys, bots with a yellow head are bots you've repaired, those are your allies, anything else is an enemy and won't hesitate to dismantle you if you get too close.
>
> You can never repair a damaged bot back to it's original strength, so your army is by design at a disadvantage in one on one battles, however you can repair them as they fight, so make sure you support them.
>
> Bots affected by your repair field have a cranking wrench next to them.
>
> There is no victory condition: you to decide when you've had enough.
> But you can always try to beat your highscore ;)

---

I was the only participant on site to use Godot (and very few had even heard of it), and given it was my first GGJ, I had some troubles finding a team, and ended up making the game on my own. I hope next time I can find a team for a different experience!

As most other teams went with Unity, I was initially willing to give in, for the sake of joining a team, but ultimately decided I wouldn't be proud of the result, nor enjoy the process.

I wanted something I could show off after the Jam, and something I would enjoy making.

I had been learning Godot for some time, and found its scene-based design to be superior to Unity's scene+prefab approach which I had the occasion to try some years before for school project.
It literally gave me the web target out of the box so I could just host the game on Gitlab Pages and anyone could play it without installing anything, and finally, it ran very well on my Linux dev box.

[Godot]: https://godotengine.org/
[GGJ Page]: https://globalgamejam.org/2020/games/repair-yourself-army-7
[source]: https://gitlab.com/ThibaultLemaire/repairyourselfanarmy
