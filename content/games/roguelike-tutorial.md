
+++
title = "Roguelike Tutorial"
date = 2020-06-27

[taxonomies]
tags = ["Rust", "WASM", "Roguelike", "RPG", "Browser-based"]
+++

[🔎 SOURCE][source] | [🕹️ PLAY](/roguelike-tutorial)

A.k.a "Tombs of the Ancient Kings", this game is a port of [the libtcod tutorial] game to [doryen-rs].

![screenshot](/roguelike-tutorial/screenshot.png)

I originally wrote this game following [this great tutorial] by [Tomas Sedovic], which in turn is a port of [the Python tutorial][the libtcod tutorial] for the [Rust] language.
I then wanted to compile it to [Webassembly] to host it on [Gitlab Pages], but couldn't because [libtcod] doesn't support the wasm target as of yet. So I ported it over to [doryen-rs]!

[the libtcod tutorial]:http://www.roguebasin.com/index.php?title=Complete_Roguelike_Tutorial,_using_python%2Blibtcod
[this great tutorial]: https://tomassedovic.github.io/roguelike-tutorial/
[Tomas Sedovic]: https://github.com/tomassedovic
[Rust]: https://www.rust-lang.org/
[Webassembly]: https://webassembly.org/
[Gitlab Pages]: https://docs.gitlab.com/ee/user/project/pages/
[libtcod]: https://github.com/libtcod/libtcod
[doryen-rs]: https://github.com/jice-nospam/doryen-rs
[source]: https://gitlab.com/ThibaultLemaire/roguelike-tutorial