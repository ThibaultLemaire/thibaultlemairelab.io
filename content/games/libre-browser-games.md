+++
title = "Libre Games You Can Play in Your Browser"
date = 2023-01-10

[taxonomies]
tags = ["Browser-based"]
+++

I did not make any of the games listed here.
Rather, this is my personal collection of Open-Source games you can play directly in your browser.

Enjoy!

Also take a look at Libre Game Wiki for additional (not necessarily browser-based) games.

[![Libregamewiki](http://libregamewiki.org/images/d/d2/Lgw2.png)](http://libregamewiki.org)

## Mad Veggies

[🔎 SOURCE](https://github.com/yopox/LD52) | [🕹️ PLAY](https://yopox.itch.io/mad-veggies)

> a puzzle game about interactions between vegetables. Some interactions are inspired from irl gardening, and some are made up to add some challenge ;)

A Ludum Dare 52 game made in Rust with the Bevy engine

## PepperTown

[🔎 SOURCE](https://github.com/cxong/PepperTown) | [🕹️ PLAY](https://congusbongus.itch.io/peppertown)

> RPG-style idle game

Features characters from Pepper & Carrot the libre web comics by David Revoy

## Triangle Run

[🔎 SOURCE](https://github.com/TerryCavanagh/triangle-run/) | [🕹️ PLAY](https://terrycavanagh.itch.io/triangle-run)

> Triangle Run is my first Godot game, created for the jam as a learning project. You play a Triangle that runs!

It's made by Terry Cavanagh no less! The guy that made Hexagon, and SuperHexagon.

## musicSnake

[🔎 SOURCE](https://github.com/KaeruCT/musicSnake) | [🕹️ PLAY](https://kaeruct.github.io/musicSnake/)

> A rhythm game with music that changes as you play.

Pretty nice, I wonder if I could repurpose some of that code for [Zen Typing](/games/zen-typing)...

## Parallel Overhead

[🔎 SOURCE](https://github.com/Huitsi/WebParallelOverhead) | [🕹️ PLAY](https://play.huitsi.net/parallel-overhead/)

> Parallel Overhead is an endless runner game where you take control of the ships Truth and Beauty on a groundbreaking trip through hyperspace. A stable hyperspace tunnel is finally being achieved with two ships locked to the opposite walls of the tunnel. Well, almost stable...

## Friday Night Funkin'

[🔎 SOURCE](https://github.com/ninjamuffin99/Funkin) | [🕹️ PLAY](https://www.newgrounds.com/portal/view/770371)

> Friday Night Funkin' is a free-to-play and open-source rhythm game. Developed by a team of four Newgrounds users, the game has a play style that is similar to Dance Dance Revolution and PaRappa the Rapper, with an aesthetic reminiscent of Flash games popular in the early-to-mid 2000s.
>
> The game revolves around the player character, simply named "Boyfriend", who has to defeat a variety of characters in singing and rapping contests for him to be able to date his love interest, "Girlfriend".
>
> — Wikipedia

> Uh oh! Your tryin to kiss ur hot girlfriend, but her MEAN and EVIL dad is trying to KILL you! He's an ex-rockstar, the only way to get to his heart? The power of music...
>
> — Itch.io

Made for the Ludum Dare 47 (2020)
