+++
title = "⁶√"
date = 2023-02-17

[taxonomies]
tags = ["Godot", "WASM", "GDScript", "Top-down", "2D", "Browser-based", "Tower-Defense", "GGJ"]
+++

[🔎 SOURCE][source] | [🕹️ PLAY][game] | [🌍 Global Game Jam][GGJ page]

[source]: https://gitlab.com/hexroot/hexroot
[game]: https://hexroot.gitlab.io/hexroot
[GGJ page]: https://globalgamejam.org/2023/games/hexroot-5

For this year's Global Game Jam (2023), I teamed up with a friend of mine and two of his colleagues.

⁶√ is a turn-based tower-defense game on a hexagonal grid with elements of economy and territory conquest.

Or at least that's how we envisioned it.

While I'm pretty satisfied with the game as it is now (0.1.0), what we had at the end of the jam (0.0.3) was... pretty disappointing to me.

I mean 0.1.0 is still missing any kind of economy system, and still has some rough edges; But 0.0.3 was more of a *Blob Simulator 2023* than a real game.
(Only the roots placing/tiling logic was implemented.
No pesky enemies to disturb you on your journey to filling the visible map with yellow roots...)

This is intriguing when compared to [last year](/games/singularity) where we had a complete and fully playable game with 8 levels by the end of the jam, whereas this year, with 2 additional people, we could barely get an interactive evocation of what we wanted the game to be.

Now, here is a non-exhaustive list of what might have happened:

1. **We were too ambitious.**
   We were 4, so each one of us could have estimated what they were capable of doing individually in that timeframe, multiplied that by 4, and made themselves an idea of how big to shoot for, but...
1. **Bigger team requires more coordination.**
   This is project management 101: You may chunk up the work however you want, there will always be a critical path that actually determines how long your project is going to take.
   And if you don't look into and organise that properly (which we didn't, duh? We came here to make a video game, not draw stupid Gantt charts...), you'll end up with people waiting on one-another more often than you'd like.
1. **We didn't go for the right kind of game.**
   The difference this year (besides the size) was that we had 2 artists in the team, and implementing the game logic quickly became the bottleneck.
   What separates dull, unfinished 0.0.3 from actually enjoyable 0.1.0, is only a week of work on my off time;
   We already had all the necessary assets (and then some) by the end of the jam!  
   I had never worked with artists before and when debriefing with them after the jam, one of them suggested we should have went with a different style of game.
   One that would lean more on the Art and less on the code, like a visual novel for example.
1. **We reinvented ~~the weel~~ hexgrids.**
   To me, another key difference with last year is that we have had to implement a bunch hexagonal grid specific logic to complement the basic features offered by Godot. Using the builtin physics of Godot for Singularity's gravity mechanics was a breathe and probably took us a cumulated 30min over the jam, compared to hours of debugging with hexgrids.
   But, hey, hexgrids are just the best, and now I know a bit more about them!
