+++
title = "About"
+++

🦊 You can find all of my projects on [Gitlab]

🐘 Follow me on [Mastodon]

🔎 Take a look at [this blog's source code]

📧 And if you like what I do, consider sending [me] a nice word, telling me what you liked

🪙 Or [buying me a cookie] 🍪 or both 🙂

---

If you or someone you know have an interesting open-source 🔓, Rust 🦀, video game 🎮, or public interest 🌍 project, I am open to work.

[me]: mailto:thibault.lemaire@protonmail.com
[Gitlab]: https://gitlab.com/ThibaultLemaire
[Mastodon]: https://fosstodon.org/@Naughtylus
[buying me a cookie]: https://liberapay.com/ThibaultLemaire/
[this blog's source code]: https://gitlab.com/ThibaultLemaire/thibaultlemaire.gitlab.io
