## My Development Environment

I use [Nix] for dependencies and [Just] for useful commands.

``` sh
nix-shell
just --list
```

[Nix]: https://nixos.org/nix/
[Just]: https://github.com/casey/just

## Building locally

1. Generate the website: `zola build`
1. Preview your project: `zola serve`

Read more at [Zola]'s [documentation].

## Host your own blog with [Zola] and [Gitlab Pages]

You can fork this project or the [example zola gitlab pages project].

[Zola]: https://getzola.org/
[documentation]: https://www.getzola.org/documentation/
[Gitlab Pages]: https://about.gitlab.com/stages-devops-lifecycle/pages/
[example zola gitlab pages project]: https://gitlab.com/JamsNJellies/zola

## License

Unless otherwise stated, the content of this repository is licensed under the MIT license, see [`LICENSE.md`](LICENSE.md).

The [favicon](favicon.svg) is licensed under the GPLv3. The original design which you can find [here][Cogwork Orthodox Church] was made by [SunnyClockwork] and is licensed under CC BY-SA 3.0. I applied a gradient made by [EliverLara] from his [Firefox icon] which is licensed under GPLv3.

[Cogwork Orthodox Church]: https://www.deviantart.com/sunnyclockwork/art/Cogwork-Orthodox-Church-557992506
[SunnyClockwork]: https://www.deviantart.com/sunnyclockwork
[EliverLara]: https://github.com/EliverLara
[Firefox icon]: https://github.com/EliverLara/candy-icons/blob/74b37f9067f30ed457386a7165b1a33efa3ecddc/apps/scalable/firefox.svg